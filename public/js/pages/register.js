$(document).ready(function () {
  (new RegisterPage).init();
});

class RegisterPage {
  init() {
    this.ROLE_SWITCHER_CHECKBOX = 'js-role-switcher';
    this.HIDDEN_FIELDS = 'js-hidden-fields';
    this.HIDDEN_BLOCK = 'js-hidden-block';

    this.initListeners();
  }

  initListeners() {
    $(`.${this.ROLE_SWITCHER_CHECKBOX}`).on('click', (e) => {
      const $switcher = $(e.currentTarget);
      const isTurnedOn = $switcher.prop('checked');
      $(`.${this.HIDDEN_BLOCK}`).fadeToggle(200);
      $(`.${this.HIDDEN_FIELDS}`).prop('required', isTurnedOn);
    });
  }
}