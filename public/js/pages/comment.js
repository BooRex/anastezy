$('.js-ta-leave-comment').on('keypress paste change', (e) => {
  const limit = 512;
  let $textarea = $(e.currentTarget);
  let text = $textarea.val();

  if (text.length >= 512) {
    const $newText = text.slice(0, limit);
    $textarea.val($newText);
    console.log('СТООП');
  }
});