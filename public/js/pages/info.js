$(document).ready(function () {
  console.log('tut');
  (new InfoPage).init();
});

export default class InfoPage {
  init() {
    this.VIEW_LINK_BTN = 'info-link';
    this.CLICK_LINK = '/info/click/';

    this.initListeners();
  }

  initListeners() {
    $(`.${this.VIEW_LINK_BTN}`).on('click', (e) => {
      const infoId = $(e.currentTarget).data('id');
      const url = `${this.CLICK_LINK}${infoId}`;

      this.sendRequest(url);
    });
  }

  sendRequest(url) {
    $.ajax({
      url: url,
      success: (data) => {
        console.log(data);
      }
    });
  }
}