<?php

namespace App\Utils;

use App\Entity\FeedBackMessage;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class StaticPagesService
 * @package App\Utils
 */
class StaticPagesService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * StaticPagesService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param null|int $status
     * @return FeedBackMessage[]
     */
    public function getFeedBacks($status = null)
    {
        if ($status) {
            $feedBacks = $this->em->getRepository(FeedBackMessage::class)->findByStatus($status);
        } else {
            $feedBacks = $this->em->getRepository(FeedBackMessage::class)->findAll();
        }

        return $feedBacks;
    }

    public function add($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function update($entity)
    {
        $this->em->merge($entity);
        $this->em->flush();
    }
}