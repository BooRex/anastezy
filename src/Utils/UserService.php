<?php

namespace App\Utils;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserService
 * @package App\Utils
 */
class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return User[]|object[]
     */
    public function getOnlyUsers()
    {
        return $this->em->getRepository(User::class)->getUserByRole(User::ROLE_USER);
    }

    public function getOne(int $id): User
    {
        return $this->em->getRepository(User::class)->find($id);
    }

    /**
     * @param User $entity
     * @return int
     */
    public function add($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        return $entity->getId();
    }

    /**
     * @param User $entity
     * @return int
     */
    public function update($entity)
    {
        $this->em->merge($entity);
        $this->em->flush();

        return $entity->getId();
    }

    /**
     * @param User $entity
     */
    public function delete($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    public function getAll()
    {
        $all = $this->em->getRepository(User::class)->findAll();
        $fullyActive = [];

        foreach ($all as $user) {
            if ($user->getStatus() !== User::STATUS_BANNED
                && $user->getStatus() !== User::STATUS_DELETED
            ) {
                $fullyActive[] = $user;
            }
        }

        return $fullyActive;
    }

    public function getDoctorLeads()
    {
        $doctors = $this->em->getRepository(User::class)->getUserByRole(User::ROLE_DOCTOR);
        $leads = [];

        /** @var User $doctor */
        foreach ($doctors as $doctor)
        {
            if ($doctor->getStatus() === User::STATUS_NOT_CONFIRMED_DOCTOR) {
                $leads[] = $doctor;
            }
        }

        return $leads;
    }

    public function getDoctors()
    {
        $doctors = $this->em->getRepository(User::class)->getUserByRole(User::ROLE_DOCTOR);
        $activeDoctors = [];

        /** @var User $doctor */
        foreach ($doctors as $doctor)
        {
            if ($doctor->getStatus() === User::STATUS_ACTIVE) {
                $activeDoctors[] = $doctor;
            }
        }

        return $activeDoctors;
    }

    public function getByEmail(string $email)
    {
        return $this->em->getRepository(User::class)->findOneBy([
            'email' => $email,
        ]);
    }
}
