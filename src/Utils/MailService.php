<?php

namespace App\Utils;

use Swift_Transport;

class MailService extends \Swift_Mailer
{
    const TITLE_PASSWORD_RECOVERY = 'Восстановление доступа к аккаунту';
    const VIEW_RESET_PASSWORD = 'emails/reset-password-template.html.twig';

    /**
     * @var TokenService
     */
    private $tokenService;

    public function __construct(Swift_Transport $transport, TokenService $tokenService)
    {
        parent::__construct($transport);
        $this->tokenService = $tokenService;

    }

    /**
     * @param string $to
     * @param $view
     * @param string $msgTitle
     * @return int
     */
    public function sendMessage(string $to, $view, string $msgTitle)
    {
        $message = (new \Swift_Message($msgTitle))
            ->setFrom(getenv('MAIL_USER'))
            ->setTo($to)
            ->setBody($view, 'text/html');

        return $this->send($message);
    }

    /**
     * @param string $baseUrl
     * @param string $tokenType
     * @param string $tokenHash
     *
     * @return string
     */
    public function generateLink(string $baseUrl, string $tokenType, string $tokenHash): string
    {
        return $baseUrl . '/token/' . $this->msgTypeTransform($tokenType) . '/' . $tokenHash;
    }

    /**
     * @param string $tokenType
     *
     * @return string
     */
    public function msgTypeTransform(string $tokenType): string
    {
        return strtolower(str_replace('_', '-', $tokenType));
    }
}
