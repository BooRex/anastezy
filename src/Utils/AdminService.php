<?php

namespace App\Utils;

use App\Entity\FeedBackMessage;
use App\Entity\Information;
use App\Entity\User;

class AdminService
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var NewsService
     */
    private $newsService;

    /**
     * @var InformationService
     */
    private $infoService;

    /**
     * @var StaticPagesService
     */
    private $staticPagesService;

    /**
     * AdminService constructor.
     *
     * @param UserService $userService
     * @param NewsService $newsService
     * @param InformationService $infoService
     * @param StaticPagesService $staticPagesService
     */
    public function __construct(
        UserService $userService,
        NewsService $newsService,
        InformationService $infoService,
        StaticPagesService $staticPagesService
    )
    {
        $this->userService = $userService;
        $this->newsService = $newsService;
        $this->infoService = $infoService;
        $this->staticPagesService = $staticPagesService;
    }

    /**
     * @return array
     */
    public function getBlocks(): array
    {
        return [
            $this->getNewsBlock(),
            $this->getUsersBlock(),
            $this->getInfoBlock(),
            $this->getDoctorLeadsBlock(),
            $this->getFeedBacksBlock(),
            $this->getStaticPagesBlock(),
        ];
    }

    /**
     * @return array
     */
    public function getNewsBlock(): array
    {
        $news = $this->newsService->getAll();

        return [
            'name'  => 'Новости',
            'color' => 'success',
            'link'  => 'admin_articles',
            'types' => [
                'Всего' => count($news),
            ]
        ];
    }

    /**
     * @return array
     */
    public function getUsersBlock(): array
    {
        $users = $this->userService->getAll();

        $countAdmins = 0;
        $countModers = 0;
        $countDoctors = 0;

        /** @var User $user */
        foreach ($users as $user) {
            switch ($user->getRoles()[0]) {
                case User::ROLE_ADMIN:
                    $countAdmins++;
                    break;
                case User::ROLE_MODERATOR:
                    $countModers++;
                    break;
                case User::ROLE_DOCTOR:
                    $countDoctors++;
                    break;
            }
        }

        return [
            'name'  => 'Пользователи',
            'color' => 'primary',
            'link'  => 'admin_users',
            'types' => [
                'Всего' => count($users),
                'Доктора' => $countDoctors,
                'Модераторы' => $countModers,
                'Администраторы' => $countAdmins,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getInfoBlock(): array
    {
        $documents = $this->infoService->getAll();

        $countConfs = 0;
        $countProtocols = 0;
        $countJournals = 0;
        $countDocMOHs = 0;

        foreach ($documents as $document) {
            switch ($document->getType()) {
                case Information::TYPE_CONFERENCE:
                    $countConfs++;
                    break;
                case Information::TYPE_PROTOCOL:
                    $countProtocols++;
                    break;
                case Information::TYPE_JOURNAL:
                    $countJournals++;
                    break;
                case Information::TYPE_DOCUMENT_MOH:
                    $countDocMOHs++;
                    break;
            }
        }

        return [
            'name'  => 'Документы',
            'color' => 'dark',
            'link'  => 'admin_docs',
            'types' => [
                'Всего' => count($documents),
                Information::TYPE_CONFERENCE_PLURAL => $countConfs,
                Information::TYPE_PROTOCOL_PLURAL => $countProtocols,
                Information::TYPE_JOURNAL_PLURAL => $countJournals,
                Information::TYPE_DOCUMENT_MOH_PLURAL => $countDocMOHs,
            ],
        ];
    }

    /**
     * @return array
     */
    private function getDoctorLeadsBlock(): array
    {
        $doctorLeads = $this->userService->getDoctorLeads();

        return [
            'name'  => 'Заявки на доктора',
            'color' => 'danger',
            'link'  => 'admin_doctor_leads',
            'types' => [
                'Всего' => count($doctorLeads),
            ],
        ];
    }

    /**
     * @return array
     */
    private function getFeedBacksBlock(): array
    {
        $feedbacks = $this->staticPagesService->getFeedBacks();
        $countFeedBacksActive = 0;
        $countFeedBacksInActive = 0;

        foreach ($feedbacks as $feedback) {
            switch ($feedback->getStatus()) {
                case FeedBackMessage::TYPE_ACTIVE:
                    $countFeedBacksActive++;
                    break;
                case FeedBackMessage::TYPE_COMPLETED:
                    $countFeedBacksInActive++;
                    break;
            }
        }

        return [
            'name'  => 'Обратная связь',
            'color' => 'warning',
            'text'  => 'dark',
            'link'  => 'admin_feedback',
            'types' => [
                'Всего'     => count($feedbacks),
                'Активных'  => $countFeedBacksActive,
                'Неактивных'  => $countFeedBacksInActive,
            ],
        ];
    }

    /**
     * @return array
     */
    private function getStaticPagesBlock(): array
    {
        return [
            'name'  => 'Статические страницы',
            'color' => 'secondary',
            'link'  => '',
            'types' => [
                'Всего' => 2,
                'Старница 1' => 'Контакты',
                'Старница 2' => 'Информация о кафедре',
            ],
        ];
    }
}