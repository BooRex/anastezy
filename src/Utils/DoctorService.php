<?php

namespace App\Utils;

use App\Entity\DoctorInfo;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DoctorService
 * @package App\Utils
 */
class DoctorService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * NewsService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getAll()
    {
        return $this->em
            ->getRepository(User::class)
            ->getUserByRole(User::ROLE_DOCTOR);
    }

    public function add(DoctorInfo $doctorInfo)
    {
        $this->em->persist($doctorInfo);
        $this->em->flush();
    }
}