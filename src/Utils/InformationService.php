<?php

namespace App\Utils;

use App\Entity\Information;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class InformationService
 * @package App\Utils
 */
class InformationService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $singularTypeValue;

    /**
     * @var string
     */
    private $pluralTypeValue;

    /**
     * InformationService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $type
     * @return array
     */
    public function getList(string $type): array
    {
        $this->defineSingularAndPluralNames($type);

        return $this->singularTypeValue
            ? $this->em
                ->getRepository(Information::class)
                ->findBy(['type' => $this->singularTypeValue])
            : [];
    }

    /**
     * @return Information[]
     */
    public function getAll() {
        return $this->em->getRepository(Information::class)->findAll();
    }

    /**
     * @return string
     */
    public function getPluralTypeValue(): string
    {
        return $this->pluralTypeValue ?? '';
    }

    /**
     * @return string
     */
    public function getSingularTypeValue(): string
    {
        return $this->singularTypeValue ?? '';
    }

    /**
     * @param Information $information
     */
    public function add(Information $information) {
        $this->em->persist($information);
        $this->em->flush();
    }

    /**
     * @param Information $information
     */
    public function update(Information $information) {
        $this->em->merge($information);
        $this->em->flush();
    }

    /**
     * @param Information $information
     */
    public function delete(Information $information) {
        $this->em->remove($information);
        $this->em->flush();
    }

    private function defineSingularAndPluralNames(string $type): void
    {
        $formattedType = str_replace('-', '_',strtoupper($type));

        $typeSingularName = sprintf('TYPE_%s', $formattedType);
        $typePluralName = sprintf('TYPE_%s_PLURAL', $formattedType);

        $this->singularTypeValue = Information::getConst($typeSingularName);
        $this->pluralTypeValue = Information::getConst($typePluralName);
    }
}