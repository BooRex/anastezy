<?php

namespace App\Utils;

use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use App\Repository\NewsRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class NewsService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * NewsService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->em->getRepository(News::class)->findAll();
    }

    /**
     * @param News $article
     * @param int $count
     */
    public function increaseViews(News $article, int $count = 1)
    {
        $currentViews = $article->getViews();
        $article->setViews($currentViews + $count);

        $this->updateArticle($article);
    }

    public function addComment(Comment $comment)
    {
        $this->em->persist($comment);
        $this->em->flush();
    }

    /**
     * @param News $article
     */
    public function addArticle(News $article) {
        $this->em->persist($article);
        $this->em->flush();
    }

    /**
     * @param News $article
     */
    public function updateArticle(News $article) {
        $this->em->merge($article);
        $this->em->flush();
    }

    /**
     * @param News $article
     */
    public function removeArticle(News $article) {
        $this->em->remove($article);
        $this->em->flush();
    }
}