<?php

namespace App\Utils;

use App\Entity\User;
use App\Entity\ConfirmationToken;
use Doctrine\ORM\EntityManagerInterface;

class TokenService
{
    private $em;

    /**
     * TokenService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $tokenType
     * @param string $tokenHash
     *
     * @return ConfirmationToken
     */
    public function get($tokenType, $tokenHash)
    {
        return $this->em->getRepository(ConfirmationToken::class)->findOneBy([
            'token' => $tokenHash,
            'tokenType' => $tokenType,
        ]);
    }

    /**
     * @param User $user
     * @param string $tokenType
     * @param \DateTime $expiredAt
     *
     * @return string
     * @throws \Exception
     */
    public function set($user, $tokenType, $expiredAt)
    {
        $confirmationToken = new ConfirmationToken();

        $tokenHash = $this->generate();

        $confirmationToken->setUser($user);
        $confirmationToken->setTokenType($tokenType);
        $confirmationToken->setToken($tokenHash);
        $confirmationToken->setExpiredAt($expiredAt);

        $this->em->persist($confirmationToken);
        $this->em->flush();

        return $tokenHash;
    }

    /**
     * @param ConfirmationToken $token
     *
     * @return bool
     * @throws \Exception
     */
    public function delete($token)
    {
        $this->em->remove($token);
        $this->em->flush();

        return true;
    }

    /**
     * @return string
     */
    public function generate()
    {
        return md5(md5(time()) . time());
    }
}
