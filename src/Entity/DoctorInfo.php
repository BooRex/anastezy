<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DoctorInfoRepository")
 */
class DoctorInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="doctorInfo", cascade={"persist", "remove"})
     */
    private $doctor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $education;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     */
    private $experience;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $achievements;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $scientific_works;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $medical_institution;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDoctor(): ?User
    {
        return $this->doctor;
    }

    public function setDoctor(?User $doctor): self
    {
        $this->doctor = $doctor;

        return $this;
    }

    public function getEducation(): ?string
    {
        return $this->education;
    }

    public function setEducation(string $education): self
    {
        $this->education = $education;

        return $this;
    }

    public function getExperience(): ?int
    {
        return $this->experience;
    }

    public function setExperience(int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getAchievements(): ?string
    {
        return $this->achievements;
    }

    public function setAchievements(?string $achievements): self
    {
        $this->achievements = $achievements;

        return $this;
    }

    public function getScientificWorks(): ?string
    {
        return $this->scientific_works;
    }

    public function setScientificWorks(?string $scientific_works): self
    {
        $this->scientific_works = $scientific_works;

        return $this;
    }

    public function getMedicalInstitution(): ?string
    {
        return $this->medical_institution;
    }

    public function setMedicalInstitution(string $medical_institution): self
    {
        $this->medical_institution = $medical_institution;

        return $this;
    }
}
