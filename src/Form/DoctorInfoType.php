<?php

namespace App\Form;

use App\Entity\DoctorInfo;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DoctorInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('education', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Образование'
                ],
            ])
            ->add('medicalInstitution', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Место работы'
                ],
            ])
            ->add('achievements', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Достижения'
                ],
            ])
            ->add('scientificWorks', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Научные работы'
                ],
            ])
            ->add('experience', NumberType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Стаж'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Отправить',
                'attr' => [
                    'class' => 'form-control btn btn-dark'
                ],
            ]);
    }

    /**
     * Setting form name
     */
    public function getBlockPrefix()
    {
        return 'change-doctor-info-form';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DoctorInfo::class,
        ]);
    }
}
