<?php

namespace App\Form;

use App\Entity\FeedBackMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeedBackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control ',
                    'placeholder' => 'Заголовок'
                ],
            ])
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control ',
                    'placeholder' => 'Ваше Имя'
                ],
            ])
            ->add('text', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control ',
                    'placeholder' => 'Сообщение..',
                    'rows' => 6,
                ],
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control ',
                    'placeholder' => 'E-mail'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Отправить',
                'attr' => ['class' => 'form-control btn btn-warning']
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FeedBackMessage::class,
        ]);
    }
}
