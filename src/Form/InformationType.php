<?php

namespace App\Form;

use App\Entity\Information;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Заголовок'
                ],
            ])
            ->add('link', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Ссылка на документ'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Отправить',
                'attr' => [
                    'class' => 'form-control btn btn-primary'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Information::class,
        ]);
    }
}