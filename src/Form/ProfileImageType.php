<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', FileType::class, [
                'data_class' => null,
                'attr' => [
                  'class' => 'custom-file-input'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Изменить',
                'attr' => [
                    'class' => 'form-control btn btn-dark'
                ],
            ]);
    }

    /**
     * Setting form name
     */
    public function getBlockPrefix()
    {
        return "upload-image-form";
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
