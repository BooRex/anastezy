<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastName', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Фамилия'
                ],
            ])
            ->add('firstName', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Имя'
                ],
            ])
            ->add('patronymic', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Отчество'
                ],
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'E-mail'
                ],
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Город'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Отправить',
                'attr' => [
                    'class' => 'form-control btn btn-dark'
                ],
            ]);
    }

    /**
     * Setting form name
     */
    public function getBlockPrefix()
    {
        return 'change-user-form';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
