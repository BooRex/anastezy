<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleEditPhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', FileType::class, [
                'data_class' => null,
                'attr' => [
                    'class' => 'custom-file-input'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Обновить картинку',
                'attr' => [
                    'class' => 'form-control btn btn-primary'
                ],
            ]);
    }

    /**
     * Setting form name
     */
    public function getBlockPrefix()
    {
        return "edit-article-photo-form";
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}