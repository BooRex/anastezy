<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', EmailType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'E-mail'
                ],
            ])
            ->add('_password', PasswordType::class, [
                'attr' => [
                    'class' => 'form-control text-center',
                    'placeholder' => 'Пароль'
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Войти',
                'attr' => ['class' => 'form-control btn btn-success']
            ]);
    }
}
