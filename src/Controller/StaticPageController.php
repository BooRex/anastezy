<?php

namespace App\Controller;

use App\Entity\FeedBackMessage;
use App\Form\FeedBackType;
use App\Utils\StaticPagesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StaticPageController extends AbstractController
{
    private $staticPagesService;

    public function __construct(StaticPagesService $staticPagesService)
    {
        $this->staticPagesService = $staticPagesService;
    }

    /**
     * @Route("/contacts", name="contacts")
     *
     * @return Response
     */
    public function showContacts()
    {
        $pageInfo = [
            'title' => 'Контакты'
        ];

        return $this->render('static_pages/contacts.html.twig', $pageInfo);
    }

    /**
     * @Route("/feedback", name="feedback")
     *
     * @param Request $request
     * @return Response
     */
    public function showFeedBack(Request $request)
    {
        $feedbackMsg = new FeedBackMessage();
        $feedbackForm = $this->createForm(FeedBackType::class, $feedbackMsg);

        if ($request->isMethod("POST")) {
            $feedbackForm->handleRequest($request);

            if ($feedbackForm->isSubmitted() && $feedbackForm->isValid()) {
                $this->staticPagesService->add($feedbackMsg);

                $this->addFlash('success-send-feedback', 'Спасибо, ожидайте ответа от администрации!');
            }
        }

        $pageInfo = [
            'feedbackForm'  => $feedbackForm->createView(),
            'title'         => 'Обратная связь'
        ];

        return $this->render('feedback/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/department-info", name="department_info")
     *
     * @return Response
     */
    public function showDepartmentInfo() {
        $pageInfo = [
            'title' => 'История кафедры',
        ];

        return $this->render('static_pages/department-info.html.twig', $pageInfo);
    }

    /**
     * @Route("/online-diagnostic", name="online_diagnostic")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showOnlineDiagnostic(Request $request): Response
    {
        $pageInfo = [
            'status'    => 404,
            'title'     => 'К сожалению по Вашему запросу, страница не найдена',
            'prevPage'  => $request->headers->get('referer'),
            'homePage'  => $this->generateUrl('news')
        ];

        return new Response(
            $this->renderView('common/error.html.twig', $pageInfo),
            404
        );
    }
}
