<?php

namespace App\Controller;

use App\Entity\Information;
use App\Utils\InformationService;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InformationController
 * @package App\Controller
 */
class InformationController extends AbstractController
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @var InformationService
     */
    private $informationService;

    /**
     * InformationController constructor.
     * @param InformationService $informationService
     * @param PaginatorInterface $paginator
     */
    public function __construct(InformationService $informationService, PaginatorInterface $paginator)
    {
        $this->informationService = $informationService;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/info/{type}", name="info")
     *
     * @param Request $request
     * @param $type
     *
     * @return Response
     */
    public function show(Request $request, string $type)
    {
        $items = $this->paginator->paginate(
            $this->informationService->getList($type),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('information/index.html.twig', [
            'items' => $items,
            'title' => $this->informationService->getPluralTypeValue(),
        ]);
    }

    /**
     * @Route("/info/click/{infoId}", name="infoLinkClick")
     * @ParamConverter("article", class="App\Entity\Information", options={"mapping": {"infoId": "id"}})
     *
     * @param Information $info
     *
     * @return Response
     */
    public function click(Information $info)
    {
        return Response::create(['check' => 5]);
    }
}
