<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\DoctorInfoType;
use App\Form\ProfileImageType;
use App\Form\ProfileType;

use App\Utils\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Route("/profile", name="profile")
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $oldImage = $user->getImage();

        $imageUploadForm = $this->createForm(ProfileImageType::class, $user);
        $changeUserForm = $this->createForm(ProfileType::class, $user);

        if ($user->getRoles()[0] === User::ROLE_DOCTOR
            && $user->getStatus() === User::STATUS_ACTIVE
        ) {
            $changeDoctorInfoFrom = $this->createForm(DoctorInfoType::class, $user->getDoctorInfo());
        }

        if ($request->isMethod('POST')) {
            if ($request->request->has('upload-image-form')) {
                $imageUploadForm->handleRequest($request);

                if ($imageUploadForm->isSubmitted() && $imageUploadForm->isValid()) {
                    // Taking image from request and put it to the image directory
                    /** @var UploadedFile $file */
                    $file = $request->files->get('upload-image-form')['image'];

                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('image_directory'), $fileName);

                    // Deleting old user image, if it exists
                    $oldImagePath = $this->getParameter('image_directory') . $oldImage;

                    if ($oldImage
                        && !empty($oldImage)
                        && file_exists($oldImagePath)
                        && $oldImage !== $this->getParameter('default_image')
                    ) {
                        unlink($oldImagePath);
                    }

                    // Setting new image into DB, when old has been deleted
                    $user->setImage($fileName);
                    $this->userService->update($user);
                }
            }

            if ($request->request->has('change-user-form')) {
                $changeUserForm->handleRequest($request);

                if ($changeUserForm->isSubmitted() && $changeUserForm->isValid()) {
                    $this->userService->update($user);
                }
            }

            if ($request->request->has('change-doctor-info-form')) {
                $changeDoctorInfoFrom->handleRequest($request);

                if ($changeDoctorInfoFrom->isSubmitted() && $changeDoctorInfoFrom->isValid()) {
                    $this->userService->update($user);
                }
            }
        }

        $pageInfo = array_filter([
            'title' => 'Профиль пользователя',
            'user' => $user,
            'imageUploadForm' => $imageUploadForm->createView(),
            'changeUserFrom' => $changeUserForm->createView(),
            'changeDoctorInfoFrom' => isset($changeDoctorInfoFrom)
                ? $changeDoctorInfoFrom->createView()
                : null
        ]);

        return $this->render('user/profile.html.twig', $pageInfo);
    }
}
