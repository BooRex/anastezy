<?php

namespace App\Controller;

use App\Entity\User;
use App\Utils\DoctorService;
use App\Utils\UserService;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DoctorController
 * @package App\Controller
 */
class DoctorController extends AbstractController
{
    /**
     * @var DoctorService
     */
    private $userService;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(UserService $userService, PaginatorInterface $paginator)
    {
        $this->userService = $userService;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/doctors", name="doctors")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAll(Request $request)
    {
        $doctors = $this->paginator->paginate(
            $this->userService->getDoctors(),
            $request->query->getInt('page', 1),
            10
        );

        $pageInfo = [
            'doctors'   => $doctors,
            'title'     => 'Реестр анастезиологов',
        ];

        return $this->render('doctors/index.html.twig', $pageInfo);
    }


    /**
     * @Route("/doctors/{doctorId}", name="doctor")
     * @ParamConverter("doctor", class="App\Entity\User", options={"mapping": {"doctorId": "id"}})
     *
     * @param User $doctor
     * @return Response
     */
    public function showOne(User $doctor) {
        $pageInfo = [
            'title'     => $doctor->getDotsName(),
            'doctor'    => $doctor,
        ];

        return $this->render('doctors/doctor/index.html.twig', $pageInfo);
    }
}