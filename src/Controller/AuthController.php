<?php

namespace App\Controller;

use App\Entity\ConfirmationToken;
use App\Entity\DoctorInfo;
use App\Entity\User;
use App\Form\LoginType;
use App\Form\RegisterType;
use App\Form\ResetPasswordType;
use App\Utils\DoctorService;
use App\Utils\MailService;
use App\Utils\TokenService;
use App\Utils\UserService;
use DateTime;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var DoctorService
     */
    private $doctorService;

    /**
     * @var TokenService
     */
    private $tokenService;

    /**
     * @var MailService
     */
    private $mailService;

    /**
     * AuthController constructor.
     * @param UserService $userService
     * @param DoctorService $doctorService
     * @param TokenService $tokenService
     * @param MailService $mailService
     */
    public function __construct(
        UserService $userService,
        DoctorService $doctorService,
        TokenService $tokenService,
        MailService $mailService
    )
    {
        $this->userService = $userService;
        $this->doctorService = $doctorService;
        $this->tokenService = $tokenService;
        $this->mailService = $mailService;
    }

    /**
     * @Route("/register", name="register")
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $role = User::ROLE_USER;
        $status = User::STATUS_ACTIVE;

        $registerForm = $this->createForm(RegisterType::class, $user);

        if ($request->isMethod("POST")) {
            if ($request->request->has("sign-up-user")) {
                $registerForm->handleRequest($request);

                if ($registerForm->isSubmitted() && $registerForm->isValid()) {
                    $doctorRequestInfo = $request->request->get('sign-up-doctor');

                    if (isset($doctorRequestInfo['role_switcher'])
                        && $doctorRequestInfo['role_switcher'] === 'on'
                    ) {
                        $doctorInfo = new DoctorInfo();
                        $role = User::ROLE_DOCTOR;
                        $status = User::STATUS_NOT_CONFIRMED_DOCTOR;

                        $doctorInfo->setMedicalInstitution($doctorRequestInfo['medical_institution']);

                        $this->doctorService->add($doctorInfo);
                        $user->setDoctorInfo($doctorInfo);
                    }

                    $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($password);
                    $user->setRoles($role);
                    $user->setStatus($status);
                    $user->setImage(User::DEFAULT_IMAGE);

                    $this->userService->add($user);

                    $this->addFlash('success-register', 'Вы успешно зарегистрированы!');
                }
            }
        }

        $pageInfo = [
            'registerForm' => $registerForm->createView(),
            'title'        => 'Регистрация'
        ];

        return $this->render('auth/register.html.twig', $pageInfo);
    }

    /**
     * @Route("/login", name="login")
     *
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();

        $loginForm = $this->createForm(LoginType::class, [
            '_username' => $authenticationUtils->getLastUsername(),
        ]);

        $pageInfo = [
            'loginForm' => $loginForm->createView(),
            'error'     => $error,
            'title'     => 'Авторизация'
        ];

        return $this->render('auth/login.html.twig', $pageInfo);
    }

    /**
     * @Route("reset-password", name="reset_password")
     *
     * @param Request $request
     * @param Swift_Mailer $mailer
     *
     * @return Response
     */
    public function resetPassword(Request $request, Swift_Mailer $mailer)
    {
        $form = $this->createForm(ResetPasswordType::class);
        $pageInfo = [
            'title' => 'Восстановление доступа',
            'form'  => $form->createView()
        ];

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $emailTo = $data['email'];
            $user = $this->userService->getByEmail($emailTo);

            if ($user) {
                $hash = $this->tokenService->set(
                    $user,
                    ConfirmationToken::PASSWORD_RECOVERY,
                    new DateTime("+1 week")
                );

                $viewParams = [
                    'user' => $user,
                    'link' => $this->mailService->generateLink(
                        $request->server->get('HTTP_ORIGIN'),
                        ConfirmationToken::PASSWORD_RECOVERY,
                        $hash
                    )
                ];

                $message = (new Swift_Message(MailService::TITLE_PASSWORD_RECOVERY))
                    ->setFrom(getenv('MAIL_USER'))
                    ->setTo($emailTo)
                    ->setBody($this->render(
                        MailService::VIEW_RESET_PASSWORD,
                        $viewParams
                    ), 'text/html');

                $mailer->send($message);
            }
        }

        return $this->render('auth/password-reset.html.twig', $pageInfo);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() {}
}
