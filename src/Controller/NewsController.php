<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\News;
use App\Form\CommentType;
use App\Utils\NewsService;
use App\Utils\UserService;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NewsController
 * @package App\Controller
 */
class NewsController extends AbstractController
{
    /**
     * @var NewsService
     */
    private $newsService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * NewsController constructor.
     * @param NewsService $newsService
     * @param UserService $userService
     * @param PaginatorInterface $paginator
     */
    public function __construct(
        NewsService $newsService,
        UserService $userService,
        PaginatorInterface $paginator
    )
    {
        $this->newsService = $newsService;
        $this->userService = $userService;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/", name="news")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAll(Request $request)
    {
        $news = $this->paginator->paginate(
            $this->newsService->getAll(),
            $request->query->getInt('page', 1),
            10
        );

        $pageInfo = [
            'news' => $news,
            'title' => 'Новости',
        ];

        return $this->render('news/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/news/{articleId}", name="article")
     * @ParamConverter("article", class="App\Entity\News", options={"mapping": {"articleId": "id"}})
     *
     * @param Request $request
     * @param News $article
     *
     * @return Response
     */
    public function showOne(Request $request, News $article)
    {
        $this->newsService->increaseViews($article);

        $comment = new Comment();

        $commentForm = $this->createForm(CommentType::class, $comment);

        if ($request->isMethod('POST')) {
            if ($request->request->has('leave-comment')) {
                $commentForm->handleRequest($request);

                if ($commentForm->isSubmitted() && $commentForm->isValid()) {
                    $author = $this->userService->getOne(
                        $request->request->get('author')
                    );

                    $comment = $commentForm->getData();
                    $comment->setNews($article);
                    $comment->setAuthor($author);

                    $this->newsService->addComment($comment);
                }
            }
        }

        $pageInfo = [
            'commentForm' => $commentForm->createView(),
            'article' => $article,
            'title' => $article->getTitle(),
        ];

        return $this->render('news/article/index.html.twig', $pageInfo);
    }
}
