<?php

namespace App\Controller;

use App\Entity\DoctorInfo;
use App\Entity\FeedBackMessage;
use App\Entity\Information;
use App\Entity\News;
use App\Entity\User;
use App\Form\ArticleEditPhotoType;
use App\Form\ArticleEditType;
use App\Form\ArticleType;
use App\Form\InformationType;
use App\Form\UserType;
use App\Utils\AdminService;
use App\Utils\InformationService;
use App\Utils\NewsService;
use App\Utils\StaticPagesService;
use App\Utils\UserService;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminPanelController
 * @package App\Controller
 *
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminPanelController extends AbstractController
{
    /**
     * @var AdminService
     */
    private $adminService;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var NewsService
     */
    private $newsService;

    /**
     * @var InformationService
     */
    private $infoService;

    /**
     * @var StaticPagesService
     */
    private $staticPagesService;

    /**
     * AdminPanelController constructor.
     * @param AdminService $adminService
     * @param UserService $userService
     * @param NewsService $newsService
     * @param InformationService $infoService
     * @param StaticPagesService $staticPagesService
     * @param PaginatorInterface $paginator
     */
    public function __construct(
        AdminService $adminService,
        UserService $userService,
        NewsService $newsService,
        InformationService $infoService,
        StaticPagesService $staticPagesService,
        PaginatorInterface $paginator
    )
    {
        $this->adminService = $adminService;
        $this->userService = $userService;
        $this->newsService = $newsService;
        $this->infoService = $infoService;
        $this->staticPagesService = $staticPagesService;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/admin", name="admin_panel")
     * @param \Swift_Mailer $mailer
     * @return Response
     */
    public function showMainPage(\Swift_Mailer $mailer)
    {
        $pageInfo = [
            'title' => 'Панель администратора',
            'blocks' => $this->adminService->getBlocks(),
        ];

        return $this->render('admin/dashboard/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/users", name="admin_users")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showUsers(Request $request)
    {
        $users = $this->userService->getAll();

        $usersPaginated = $this->paginator->paginate(
            $users,
            $request->query->getInt('page', 1),
            10
        );

        $pageInfo = [
            'title' => 'Управление пользователями',
            'users' => $usersPaginated,
            'totalCount' => count($users),
        ];

        return $this->render('admin/users/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/users/edit/{userId}", name="admin_edit_user")
     * @ParamConverter("user", class="App\Entity\User", options={"mapping": {"userId": "id"}})
     *
     * @param Request $request
     * @param User $user
     *
     * @return Response
     */
    public function editUser(Request $request, User $user)
    {
        $form = $this->createForm(UserType::class, $user);

        if ($request->isMethod("POST")) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();

                $user->setRoles($request->request->get('role'));
                $this->userService->update($user);

                $this->addFlash('success-edit', 'Пользователь успешно обновлен.');

                return $this->redirectToRoute('admin_users');
            }
        }

        $pageInfo = [
            'title' => 'Изменение пользователя',
            'user' => $user,
            'form' => $form->createView(),
        ];

        return $this->render('admin/users/edit.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/users/delete/{userId}", name="admin_delete_user")
     * @ParamConverter("user", class="App\Entity\User", options={"mapping": {"userId": "id"}})
     *
     * @param User $user
     * @return Response
     */
    public function deleteUser(User $user)
    {
        $user->setStatus(User::STATUS_DELETED);
        $this->userService->update($user);
        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/admin/docs", name="admin_docs")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showDocs(Request $request)
    {
        $docs = $this->infoService->getAll();

        $docsPaginated = $this->paginator->paginate(
            $docs,
            $request->query->getInt('page', 1),
            10
        );

        $pageInfo = [
            'title' => 'Документы',
            'docs' => $docsPaginated,
            'totalCount' => count($docs),
        ];

        return $this->render('admin/docs/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/docs/add/", name="admin_add_doc")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addDoc(Request $request)
    {
        $doc = new Information();
        $form = $this->createForm(InformationType::class, $doc);

        if ($request->isMethod("POST")) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var Information $doc */
                $doc = $form->getData();
                $doc->setType($request->request->get('type'));
                $this->infoService->add($doc);

                $this->addFlash('success-edit', 'Документ успешно создан.');

                return $this->redirectToRoute('admin_docs');
            }
        }

        $pageInfo = [
            'title' => 'Создание документа',
            'doc' => $doc,
            'form' => $form->createView(),
        ];

        return $this->render('admin/docs/edit.html.twig', $pageInfo);
    }


    /**
     * @Route("/admin/docs/edit/{docId}", name="admin_edit_doc")
     * @ParamConverter("doc", class="App\Entity\Information", options={"mapping": {"docId": "id"}})
     *
     * @param Request $request
     * @param Information $doc
     *
     * @return Response
     */
    public function editDoc(Request $request, Information $doc)
    {
        $form = $this->createForm(InformationType::class, $doc);

        if ($request->isMethod("POST")) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var Information $doc */
                $doc = $form->getData();
                $doc->setType($request->request->get('type'));
                $this->infoService->update($doc);

                $this->addFlash('success-edit', 'Документ успешно обновлен.');
                return $this->redirectToRoute('admin_docs');
            }
        }

        $pageInfo = [
            'title' => 'Изменение документа',
            'doc' => $doc,
            'form' => $form->createView(),
        ];

        return $this->render('admin/docs/edit.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/docs/delete/{docId}", name="admin_delete_doc")
     * @ParamConverter("doc", class="App\Entity\Information", options={"mapping": {"docId": "id"}})
     *
     * @param Information $doc
     * @return Response
     */
    public function deleteDoc(Information $doc)
    {
        $this->infoService->delete($doc);
        return $this->redirectToRoute('admin_docs');
    }

    /**
     * @Route("/admin/feedback", name="admin_feedback")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showFeedBackRequests(Request $request)
    {
        $feedBacks = $this->staticPagesService->getFeedBacks(FeedBackMessage::TYPE_ACTIVE);

        $feedBacksPaginated = $this->paginator->paginate(
            $feedBacks,
            $request->query->getInt('page', 1),
            10
        );

        $pageInfo = [
            'title' => 'Сообщения от пользователей',
            'items' => $feedBacksPaginated,
            'totalCount' => count($feedBacks),
        ];

        return $this->render('admin/feedbacks/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/feedback/close/{feedbackId}", name="admin_feedback_close")
     * @ParamConverter("feedback", class="App\Entity\FeedBackMessage", options={"mapping": {"feedbackId": "id"}})
     *
     * @param FeedBackMessage $feedback
     *
     * @return Response
     */
    public function closeFeedBackRequest(FeedBackMessage $feedback)
    {
        $feedback->setStatus(FeedBackMessage::TYPE_COMPLETED);
        $this->staticPagesService->update($feedback);

        return $this->redirectToRoute('admin_feedback');
    }

    /**
     * @Route("/admin/doctor-leads", name="admin_doctor_leads")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showDoctorLeads(Request $request)
    {
        $leads = $this->userService->getDoctorLeads();

        $leadsPaginated = $this->paginator->paginate(
            $leads,
            $request->query->getInt('page', 1),
            10
        );

        $pageInfo = [
            'title' => 'Заявки на доктора',
            'items' => $leadsPaginated,
            'totalCount' => count($leads),
        ];

        return $this->render('admin/doctor_leads/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/doctor-leads/not-approve/{doctorId}", name="admin_doctor_leads_not_approve")
     * @ParamConverter("doctor", class="App\Entity\User", options={"mapping": {"doctorId": "id"}})
     *
     * @param User $doctor
     *
     * @return Response
     */
    public function notApproveDoctorLead(User $doctor)
    {
        $doctor->setStatus(User::STATUS_ACTIVE);
        $doctor->setRoles(User::ROLE_USER);
        $doctor->setDoctorInfo(new DoctorInfo());

        $this->userService->update($doctor);

        return $this->redirectToRoute('admin_doctor_leads');
    }

    /**
     * @Route("/admin/doctor-leads/approve/{doctorId}", name="admin_doctor_leads_approve")
     * @ParamConverter("doctor", class="App\Entity\User", options={"mapping": {"doctorId": "id"}})
     *
     * @param User $doctor
     *
     * @return Response
     */
    public function approveDoctorLead(User $doctor)
    {
        $doctor->setStatus(User::STATUS_ACTIVE);

        $this->userService->update($doctor);

        return $this->redirectToRoute('admin_doctor_leads');
    }

    /**
     * @Route("/admin/news", name="admin_articles")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showArticles(Request $request)
    {
        $articles = $this->newsService->getAll();

        $articlesPaginated = $this->paginator->paginate(
            $articles,
            $request->query->getInt('page', 1),
            10
        );

        $pageInfo = [
            'title' => 'Статьи',
            'articles' => $articlesPaginated,
            'totalCount' => count($articles),
        ];

        return $this->render('admin/news/index.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/news/add/", name="admin_add_article")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addArticle(Request $request)
    {
        $article = new News();
        $form = $this->createForm(ArticleType::class, $article);

        if ($request->isMethod("POST")) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var News $article */
                $article = $form->getData();

                /** @var UploadedFile $file */
                $file = $request->files->get('create-article-form')['image'];

                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('news_image_directory'), $fileName);

                $article->setImage($fileName);
                $article->setViews(0);
                $article->setAuthor($this->getUser());

                $this->newsService->addArticle($article);

                $this->addFlash('success-edit', 'Статья успешно создана.');

                return $this->redirectToRoute('admin_articles');
            }
        }

        $pageInfo = [
            'title' => 'Создание статьи',
            'article' => $article,
            'form' => $form->createView(),
        ];

        return $this->render('admin/news/add.html.twig', $pageInfo);
    }


    /**
     * @Route("/admin/news/edit/{articleId}", name="admin_edit_article")
     * @ParamConverter("article", class="App\Entity\News", options={"mapping": {"articleId": "id"}})
     *
     * @param Request $request
     * @param News $article
     *
     * @return Response
     */
    public function editArticle(Request $request, News $article)
    {
        $oldImage = $article->getImage();

        $form = $this->createForm(ArticleEditType::class, $article);
        $formImg = $this->createForm(ArticleEditPhotoType::class, $article);

        if ($request->isMethod("POST")) {
            if ($request->request->has('edit-article-form')) {
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    /** @var News $article */
                    $article = $form->getData();

                    $this->newsService->updateArticle($article);

                    $this->addFlash('success-edit', 'Статья успешно обновлена.');
                    return $this->redirectToRoute('admin_articles');
                }
            }

            if ($request->request->has('edit-article-photo-form'))
            {
                $formImg->handleRequest($request);

                if ($formImg->isSubmitted() && $formImg->isValid()) {
                    // Taking image from request and put it to the image directory
                    /** @var UploadedFile $file */
                    $file = $request->files->get('edit-article-photo-form')['image'];

                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('news_image_directory'), $fileName);

                    // Deleting old user image, if it exists
                    $oldImagePath = $this->getParameter('news_image_directory') . $oldImage;

                    if ($oldImage
                        && !empty($oldImage)
                        && file_exists($oldImagePath)
                        && $oldImage !== $this->getParameter('news_default_image')
                    ) {
                        unlink($oldImagePath);
                    }

                    // Setting new image into DB, when old has been deleted
                    $article->setImage($fileName);
                    $this->newsService->updateArticle($article);
                }
            }
        }

        $pageInfo = [
            'title'     => 'Изменение статьи',
            'article'   => $article,
            'form'      => $form->createView(),
            'formImg'   => $formImg->createView(),
        ];

        return $this->render('admin/news/edit.html.twig', $pageInfo);
    }

    /**
     * @Route("/admin/news/delete/{articleId}", name="admin_delete_article")
     * @ParamConverter("article", class="App\Entity\News", options={"mapping": {"articleId": "id"}})
     *
     * @param News $article
     * @return Response
     */
    public function deleteArticle(News $article)
    {
        $this->newsService->removeArticle($article);
        return $this->redirectToRoute('admin_articles');
    }
}
