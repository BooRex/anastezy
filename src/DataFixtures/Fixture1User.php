<?php

namespace App\DataFixtures;

use App\Entity\DoctorInfo;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Fixture1User extends Fixture
{
    public const DEFAULT_IMAGE = 'default-profile.png';

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * Fixture1User constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param ObjectManager $om
     */
    public function load(ObjectManager $om): void
    {
        $this->createCustomEntities($om);

        $this->create($om, 10, 123, User::ROLE_USER);
        $this->create($om, 20, 123, User::ROLE_DOCTOR);
        $this->create($om, 3,1234, User::ROLE_MODERATOR);
        $this->create($om, 1, 12345, User::ROLE_ADMIN);
    }

    /**
     * @param ObjectManager $om
     * @param int $count
     * @param string $pass
     * @param string $role
     */
    private function create(
        ObjectManager $om,
        int $count,
        string $pass,
        string $role
    ): void
    {
        for ($i = 0; $i < $count; $i++) {
            $user = new User();
            $user->setEmail(sprintf('mail_%s_%s@gmail.com', $role, $i));
            $user->setFirstName(sprintf('Name%s', $i));
            $user->setLastName(sprintf('LastName%s', $i));
            $user->setPatronymic(sprintf('Patronymic%s', $i));
            $user->setPassword($this->encoder->encodePassword($user, $pass));
            $user->setRoles($role);
            $user->setImage(self::DEFAULT_IMAGE);
            $user->setStatus(User::STATUS_ACTIVE);
            $user->setCity('Харьков');

            if ($role === User::ROLE_DOCTOR) {
                $doctorInfo = $this->generateDoctorInfo($user);
                $user->setDoctorInfo($doctorInfo);
            }

            $om->persist($user);
            $om->flush();
        }
    }

    /**
     * @param ObjectManager $om
     */
    private function createCustomEntities(ObjectManager $om): void
    {
        $this->addCustom($om, 'Александр', 'Туник', 'Евгеньевич', 'alex.tunik@gmail.com', 'secret123', User::ROLE_ADMIN);
        $this->addCustom($om, 'Олег', 'Бритвин', 'Викторович', 'boorex21997@gmail.com', 'secret123', User::ROLE_ADMIN);
    }

    /**
     * @param User $user
     *
     * @return DoctorInfo
     */
    private function generateDoctorInfo(User $user): DoctorInfo
    {
        $doctorInfo = new DoctorInfo();

        $doctorInfo->setDoctor($user);
        $doctorInfo->setEducation('ХНМУ им. Анастезилогов');
        $doctorInfo->setAchievements('Очень много достижений, лечение рака простой водой и солью');
        $doctorInfo->setExperience(20);
        $doctorInfo->setMedicalInstitution('Обласная больница №1');
        $doctorInfo->setScientificWorks('Учение об наркотиках, Книга "Как отключить человека за 10 секунд", Сборник знаний по анастезиологии, Сборник знаний по космическим путешествиям');

        return $doctorInfo;
    }

    /**
     * @param ObjectManager $om
     * @param string $firstName
     * @param string $lastName
     * @param string $patronymic
     * @param string $email
     * @param string $password
     * @param string $role
     */
    private function addCustom(
        ObjectManager $om,
        string $firstName,
        string $lastName,
        string $patronymic,
        string $email,
        string $password,
        string $role
    ): void
    {
        $user = new User();

        $user->setEmail($email);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setPatronymic($patronymic);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setRoles($role);
        $user->setImage(self::DEFAULT_IMAGE);
        $user->setStatus(User::STATUS_ACTIVE);

	    $om->persist($user);
        $om->flush();
    }
}
