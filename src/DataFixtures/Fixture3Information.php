<?php

namespace App\DataFixtures;

use App\Entity\Information;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixture3Information extends Fixture
{
    public const DEFAULT_TITLE = '%s #%s по каким-то вопросам и проблемам, чисто для наполнения сайта. Еще пару строчек о чем-то важном';
    public const DEFAULT_CONFERENCE_LINK = 'https://www.youtube.com/watch?v=haJcvr3IfqE';
    public const DEFAULT_FILE_LINK = 'http://www.soundczech.cz/temp/lorem-ipsum.pdf';

    public function load(ObjectManager $om): void
    {
        $this->createCustomData($om);

        // Uncomment if you want to use default data
//        $this->createDefaultData($om);
    }

    /**
     * @param ObjectManager $om
     * @param int $count
     * @param string $type
     * @param string $link
     */
    private function createDefaultSet(
        ObjectManager $om,
        int $count,
        string $type,
        string $link
    ): void
    {
        for ($i = 0; $i < $count; $i++) {
            $this->create(
                $om,
                sprintf(self::DEFAULT_TITLE, $type, $i),
                $link,
                $type
            );
        }
    }

    /**
     * @param ObjectManager $om
     */
    private function createCustomData(ObjectManager $om): void
    {
        $this->create(
            $om,
            'Наказ МОЗ України від 21.07.2017 № 834 "Про затвердження складу груп експертів МОЗ України"',
            'http://moz.gov.ua/docfiles/dn_20170721_834_dod.pdf#page=2',
            'Документ МОЗ'
        );
        $this->create(
            $om,
            'Про затвердження Положення про групи експертів МОЗ України',
            'http://zakon2.rada.gov.ua/laws/show/z0488-17',
            'Документ МОЗ'
        );
        $this->create(
            $om,
            'Наказ МОЗ України від 29 грудня  2016 року No 1422. "Зміни до методики розробки та впровадження медичних стандартів  (уніфікованих клінічних протоколів) медичної допомоги на засадах  доказової медицини"',
            'http://old.moz.gov.ua/ua/portal/dn_20161229_1422.html',
            'Документ МОЗ'
        );
        $this->create(
            $om,
            'Про внесення змін до Положення про порядок проведення атестації лікарів',
            'http://zakon0.rada.gov.ua/laws/show/z0176-16',
            Information::TYPE_DOCUMENT_MOH
        );
        $this->create(
            $om,
            'Про деякі питання придбання, перевезення, зберігання, відпуску, використання та знищення наркотичних засобів, психотропних речовин і прекурсорів у закладах охорони здоров’я',
            'http://zakon2.rada.gov.ua/laws/show/z1028-15#n151',
            Information::TYPE_DOCUMENT_MOH
        );
        $this->create(
            $om,
            'Про створення та впровадження медико-технологічних документів зі стандартизації медичної допомоги в системі Міністерства охорони здоровя України ',
            'http://moz.gov.ua/ua/portal/dn_20120928_751.html',
            Information::TYPE_DOCUMENT_MOH
        );
        $this->create(
            $om,
            'Про затвердження Змін до Положення про проведення іспитів на передатестаційних циклах ',
            'http://old.moz.gov.ua/ua/portal/dn_20090707_484.html',
            Information::TYPE_DOCUMENT_MOH
        );
        $this->create(
            $om,
            'Про вдосконалення профілактики, діагностики та лікування правця ',
            'http://mozdocs.kiev.ua/view.php?id=776',
            Information::TYPE_DOCUMENT_MOH
        );
        $this->create(
            $om,
            'Уніфікований клінічний протокол медичної допомоги  "Контроль періопераційного болю"',
            'https://drive.google.com/file/d/0BybNTYCcMs2ZUWFEdXNZa3k5UlE/view?usp=drive_open',
            Information::TYPE_PROTOCOL
        );
        $this->create(
            $om,
            'Уніфікований клінічний протокол екстреної',
            'http://www.webcardio.org/unifikovanyj-klinichnyj-protokol-ekstrenoji-pervynnoji-vtorynnoji-tretynnoji-medychnoji-dopomoghy-ta-medychnoji-reabilitatsiji-ghostryj-koronarnyj-syndrom-bez-elevatsiji-seghmenta-st.aspx',
            Information::TYPE_PROTOCOL
        );
    }

    /**
     * @param ObjectManager $om
     * @param string $title
     * @param string $link
     * @param string $type
     */
    private function create(
        ObjectManager $om,
        string $title,
        string $link,
        string $type
    ): void
    {
        $info = new Information();

        $info->setTitle($title);
        $info->setLink($link);
        $info->setType($type);

        $om->persist($info);
        $om->flush();
    }

    /**
     * @param ObjectManager $om
     */
    private function createDefaultData(ObjectManager $om): void
    {
        $this->createDefaultSet($om, 20, Information::TYPE_CONFERENCE, self::DEFAULT_CONFERENCE_LINK);
        $this->createDefaultSet($om, 20, Information::TYPE_DOCUMENT_MOH, self::DEFAULT_FILE_LINK);
        $this->createDefaultSet($om, 20, Information::TYPE_JOURNAL, self::DEFAULT_FILE_LINK);
        $this->createDefaultSet($om, 20, Information::TYPE_PROTOCOL, self::DEFAULT_FILE_LINK);
    }
}
