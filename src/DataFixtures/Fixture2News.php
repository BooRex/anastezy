<?php

namespace App\DataFixtures;

use App\Entity\News;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixture2News extends Fixture
{
    public const DEFAULT_TEXT = 'Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе. В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus Bonorum et Malorum" ("О пределах добра и зла"), написанной Цицероном в 45 году н.э. Этот трактат по теории этики был очень популярен в эпоху Возрождения. Первая строка Lorem Ipsum, "Lorem ipsum dolor sit amet..", происходит от одной из строк в разделе 1.10.32. Классический текст Lorem Ipsum, используемый с XVI века, приведён ниже. Также даны разделы 1.10.32 и 1.10.33 "de Finibus Bonorum et Malorum" Цицерона и их английский перевод, сделанный H. Rackham, 1914 год.';
    public const DEFAULT_IMAGE = 'default-article-image.png';

    /**
     * @param ObjectManager $om
     */
    public function load(ObjectManager $om): void
    {
        $this->create($om, 100, 'Новость', self::DEFAULT_TEXT);
    }

    /**
     * @param ObjectManager $om
     * @param int $count
     * @param string $title
     * @param string $text
     */
    private function create(
        ObjectManager $om,
        int $count,
        string $title,
        string $text
    ): void
    {
        /** @var User $author */
        $author = $om->find(User::class, 25);

        for ($i = 0; $i < $count; $i++) {
            $article = new News();

            $article->setAuthor($author);
            $article->setTitle(sprintf('%s_%s', $title, $i));
            $article->setText(sprintf('%s_%s', $text, $i));
            $article->setImage(self::DEFAULT_IMAGE);
            $article->setViews(0);

            $om->persist($article);
            $om->flush();
        }
    }
}
