<?php

namespace App\Repository;

use App\Entity\FeedBackMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FeedBackMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeedBackMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeedBackMessage[]    findAll()
 * @method FeedBackMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedBackMessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FeedBackMessage::class);
    }

    /**
     * @param int $value
     * @return FeedBackMessage[] Returns an array of FeedBackMessage objects
     */
    public function findByStatus(int $value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.status = :val')
            ->setParameter('val', $value)
            ->orderBy('f.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
