<?php

namespace App\Repository;

use App\Entity\DoctorInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DoctorInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method DoctorInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method DoctorInfo[]    findAll()
 * @method DoctorInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctorInfoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DoctorInfo::class);
    }
}
